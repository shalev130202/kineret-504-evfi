import cv2
import os
import numpy as np
import removeLines as rl

saveImageDir="slicingOutput/"
imagequeue="ImageQueue/"
def Binarization(img):
    #clear noise
    src = cv2.imread(img, 0)
    GaussianBlur = cv2.GaussianBlur(src, (5, 5), cv2.BORDER_DEFAULT)
    inpaintMask = cv2.imread(img, cv2.CV_8UC1)
    tresh, inpaintMask = cv2.threshold(inpaintMask, 170, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
    return inpaintMask

def RemoveNoise(img):

    gray = img
    original=img.copy()
    nlabel, labels, stats, centroids = cv2.connectedComponentsWithStats(gray, connectivity=8)
    thresh_size =  20
    for i in range(1, nlabel):
        #
        if (stats[i, cv2.CC_STAT_WIDTH] > thresh_size) or (stats[i, cv2.CC_STAT_HEIGHT] > thresh_size):
            gray[labels == i] = 0
        if stats[i, cv2.CC_STAT_AREA] < 4:
            gray[labels == i] = 0

    cv2.waitKey()
    dst = cv2.inpaint(original, gray, 3, cv2.INPAINT_TELEA)
    dst[:, 0:30] = 0
    dst[0:10, :] = 0
    return dst
def sortCentroids(dict):
    return sortWords(sortLines(dict))

def sortLines(dict):
    sortDict=sorted(dict.items(), key=lambda dict: dict[1][1])
    lines=[]
    line={}
    flag=False
    prev=int(sortDict[0][1][1])

    for item in sortDict:

        if(prev==int(item[1][1]) or prev-30 <= int(item[1][1]) <= prev+30 ):
            line[item[0]]=item[1]
            prev = int(item[1][1])
        else:
            lines.append(line)
            line={}
            line[item[0]]=item[1]
            prev = int(item[1][1])

    lines.append(line)
    return  lines
def sortWords(listLines):
    sortedLines=[]
    line=[]
    for line in listLines:
        lineSort = []
        sortLine = sorted(line.items(), key=lambda line: line[1][0])
        for list in sortLine:
            lineSort.append(list[0])
        sortedLines.append(lineSort)
        lineSort = []
    return sortedLines

def ExtractLetters(img):
    imglst = []
    sortedimglst=[]
    centroidDict=dict()
    imglst.append(np.zeros(img.shape, dtype=np.uint8))
    kernel = np.ones((20, 3), np.uint8)
    dilate=cv2.dilate(img, kernel, iterations=1)
    nlabel, labels,stats,centroids = cv2.connectedComponentsWithStats(dilate)
    i=0
    for centroid in centroids:
        centroidDict[i]=centroid
        i += 1
    centroidDict = sorted(centroidDict.items(), key=lambda centroidDict: centroidDict[1][0])
    for i in range(1, nlabel):
        imglst.append(np.zeros(img.shape, dtype=np.uint8))
    for row in range(0, (labels.shape[0])):
        for col in range(0, (labels.shape[1])):
            lab = labels[row][col]

            imglst[lab][row][col] = img[row][col]
    for cor in centroidDict:
        if cor[0]==0:
            continue
        else:
            sortedimglst.append(imglst[cor[0]])
    sortedimglst.reverse()
    return sortedimglst


def ExtractWords(img,FileName):

    centroidDict = dict()
    imglst = []
    letterlst=[]
    result=[]
    imglst.append(np.zeros(img.shape, dtype=np.uint8))
    ret, thresh = cv2.threshold(img, 50, 255, cv2.THRESH_BINARY)

    img=thresh
    kernel = np.ones((3, 50), np.uint8)
    dilate = cv2.dilate(img, kernel, iterations=1)

    nlabel, labels,stats,centroids = cv2.connectedComponentsWithStats(dilate)
    i=0
    for centroid in centroids:

        centroidDict[i]=centroid
        i += 1
    words=sortCentroids(centroidDict)


    old=img


    for i in range(1, nlabel):
            imglst.append(np.zeros(img.shape, dtype=np.uint8))
    for row in range(0, (labels.shape[0])):
        for col in range(0, (labels.shape[1])):
            lab = labels[row][col]

            imglst[lab][row][col] = old[row][col]
    for line in words:
        line=line.reverse()

    print("finish sort")
    i=0
    w=0
    for line in words:

        for word in line:
            lettersimg=[]
            sum=np.sum(imglst[word])

            if(np.sum(imglst[word])<50000):
                continue

            if np.sum(imglst[word]) !=0:

                firstimg=imglst[word]
                originalimg=imglst[word]
                kernel = np.ones((15, 1), np.uint8)
                dilate = cv2.dilate(firstimg, kernel, iterations=1)
                firstimg=dilate
                nonzero = np.nonzero(firstimg)

                minx = min(nonzero[1])
                maxx = max(nonzero[1])

                miny = min(nonzero[0])
                maxy = max(nonzero[0])

                crop= firstimg[miny:maxy, minx:maxx]
                original=originalimg[miny:maxy, minx:maxx]
                try:
                    letterlst = ExtractLetters(original)
                except:
                    continue
                x_offset = 450
                y_offset = 800
                for letters in range(0,len(letterlst)):
                    firstimg = letterlst[letters]
                    nonzero = np.nonzero(firstimg)

                    minx = min(nonzero[1])
                    maxx = max(nonzero[1])

                    miny = min(nonzero[0])
                    maxy = max(nonzero[0])

                    crop = firstimg[miny:maxy, minx:maxx]
                    resizecounter=0
                    if crop.shape[0]>1 and crop.shape[1]>1:
                        #while (crop.shape[0]<8 or crop.shape[1]<10):
                         #   crop=cv2.resize(crop, (int(crop.shape[1]*1.2+0.9),int(crop.shape[0]*1.2+0.9)), interpolation=cv2.INTER_AREA)
                          #  resizecounter+=1
                           # if (crop.shape[0] > 35 or crop.shape[1]>35 or resizecounter==3):
                            #    break

                        #print(crop.shape)

                        blank_image = np.zeros((1600, 900), np.uint8)

                        blank_image[y_offset:y_offset + crop.shape[0], x_offset:x_offset + crop.shape[1]] = crop
                        blank_image=255-blank_image
                        cv2.imwrite(saveImageDir +FileName+ "/w" + str(w)+"l"+str(letters) +".jpg",blank_image)

            w+=1


        i += 1
        open(saveImageDir +FileName+ "/w"+ str(w), "w").close()


def processImage(imageName):
    try:
        os.mkdir(saveImageDir + imageName)
    except:
        return 1
    binarization = Binarization(imagequeue + imageName)
    ret = rl.removeLines(binarization)

    resized = cv2.resize(ret, (450,800), interpolation=cv2.INTER_AREA)


    ret = cv2.cvtColor(ret, cv2.COLOR_BGR2GRAY)
    kernel = np.ones((5, 1), np.uint8)
    ret = cv2.dilate(ret, kernel, iterations=1)

    ret=RemoveNoise(ret)
    resized = cv2.resize(ret, (450,800), interpolation=cv2.INTER_AREA)

    ret = cv2.erode(ret, kernel, iterations=1)
    ExtractWords(ret, imageName)
    return  0



if __name__ == '__main__':
    main()
