                                                                                                                 # test.py

import numpy as np
import os
import tensorflow as tf
import cv2
import math

from utils import label_map_util
from utils import visualization_utils as vis_util
from distutils.version import StrictVersion


import base64
print(vis_util.__file__)
classstr=[]
boxesstr=[]

# module level variables ##############################################################################################
TEST_IMAGE_DIR = os.getcwd() +"\\test_images"
FROZEN_INFERENCE_GRAPH_LOC = os.getcwd() + "/inference_graph/frozen_inference_graph.pb"
LABELS_LOC = os.getcwd() + "/" + "label_map.pbtxt"
NUM_CLASSES = 28
letters={"alef":"א","bet":"ב","gimel":"ג","dalet":"ד","hai":"ה","vav":"ו","zain":"ז","het":"ח","tet":"ט","yod":"י","chaf":"כ","lamed":"ל","mem":"מ","non":"נ",
         "samech":"ס","ain":"ע","pai":"פ","chadic":"צ","kof":"ק","resh":"ר","shin":"ש","taf":"ת","nons":"ן","mems":"ם","pais":"ף","chadics":"ץ","chafs":"ך"}
cword=[]
def treshHold(img):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)  # Convert to Grayscale.

    ret, thresh = cv2.threshold(gray, 40, 255,
                                cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)  # Convert to binary and invert polarity

    # Use "close" morphological operation to close small gaps
    thresh = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, np.array([1, 1]));
    thresh = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, np.array([1, 1]).T);

    nlabel, labels, stats, centroids = cv2.connectedComponentsWithStats(thresh, connectivity=8)

    thresh_size = 400

    # Delete all lines by filling wide and long lines with zeros.
    # Delete very small clusters (assumes to be noise).
    for i in range(1, nlabel):
        #
        if (stats[i, cv2.CC_STAT_WIDTH] > thresh_size) or (stats[i, cv2.CC_STAT_HEIGHT] > thresh_size):
            thresh[labels == i] = 0
        if stats[i, cv2.CC_STAT_AREA] < 4:
            thresh[labels == i] = 0

    # Clean left and top margins "manually":


    # Inverse polarity
    thresh = 255 - thresh
    return thresh
def rotateImage(image, angle):

    center=tuple(np.array(image.shape[0:2])/2)
    rot_mat = cv2.getRotationMatrix2D(center,angle,1.0)
    return cv2.warpAffine(image, rot_mat, image.shape[0:2],flags=cv2.INTER_LINEAR)
def resizeImg(imgName):
    img = imgName
    size=img.shape[:2]

    scale_percent = 1.5  # percent of original size
    width = int(img.shape[1] / scale_percent )
    height = int(img.shape[0] / scale_percent )
    dim = (width, height)
    # resize image
    resized = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)

    return resized

#######################################################################################################################
def main(dir):
    TEST_IMAGE_DIR = "slicingOutput/"+dir
    trm=0
    rotetCount=0
    imageCount=0
    flag = True
    print("starting program . . .")
    retBoxes=[]
    #if not checkIfNecessaryPathsAndFilesExist():
     #   return
    # end if

    # this next comment line is necessary to avoid a false PyCharm warning
    # noinspection PyUnresolvedReferences
    if StrictVersion(tf.__version__) < StrictVersion('1.5.0'):
        raise ImportError('Please upgrade your tensorflow installation to v1.5.* or later!')
    # end if

    # load a (frozen) TensorFlow model into memory
    detection_graph = tf.Graph()
    with detection_graph.as_default():
        od_graph_def = tf.GraphDef()
        with tf.gfile.GFile(FROZEN_INFERENCE_GRAPH_LOC, 'rb') as fid:
            serialized_graph = fid.read()
            od_graph_def.ParseFromString(serialized_graph)
            tf.import_graph_def(od_graph_def, name='')
        # end with
    # end with

    # Loading label map
    # Label maps map indices to category names, so that when our convolution network predicts `5`, we know that this corresponds to `airplane`.  Here we use internal utility functions, but anything that returns a dictionary mapping integers to appropriate string labels would be fine
    label_map = label_map_util.load_labelmap(LABELS_LOC)
    categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES,
                                                                use_display_name=True)
    category_index = label_map_util.create_category_index(categories)

    imageFilePaths = []
    for imageFileName in os.listdir(TEST_IMAGE_DIR):

        imageFilePaths.append(TEST_IMAGE_DIR + "/" + imageFileName)
        # end if
    # end for
    last=-1
    with detection_graph.as_default():

        with tf.Session(graph=detection_graph) as sess:
            for image_path in imageFilePaths:


                imageCount+=1
                start=100
                classstr = []
                boxesstr = []
                letters=[]
                words = []

                #print(image_path)
                with open(image_path, "rb") as imageFile:
                    str = base64.b64encode(imageFile.read())

                image_np = cv2.imread(image_path)
                originalPhoto=image_np

                if image_np is None:
                    retBoxes.append([""])
                    continue
                curr = int(image_path.split("w")[1][0])
                if curr != last:
                    retBoxes.append([" "])
                    last = curr
                else:
                    last = curr
                # end if

                # Definite input and output Tensors for detection_graph
                image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
                # Each box represents a part of the image where a particular object was detected.
                detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
                # Each score represent how level of confidence for each of the objects.
                # Score is shown on the result image, together with the class label.
                detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
                detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')
                num_detections = detection_graph.get_tensor_by_name('num_detections:0')

                # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
                image_np_expanded = np.expand_dims(image_np, axis=0)
                # Actual detection.
                (boxes, scores, classes, num) = sess.run(
                    [detection_boxes, detection_scores, detection_classes, num_detections],
                    feed_dict={image_tensor: image_np_expanded})
                # Visualization of the results of a detection.


                image,boxestr=vis_util.visualize_boxes_and_labels_on_image_array(image_np,
                                                                   np.squeeze(boxes),
                                                                   np.squeeze(classes).astype(np.int32),
                                                                   np.squeeze(scores),
                                                                   category_index,
                                                                   use_normalized_coordinates=True,
                                                                 line_thickness=2)

                boxestrvlist=list(boxestr.values())
                ####resize until box found






                #print result
                #cv2.imshow("image_np", image_np)
                #cv2.waitKey()
                if(boxestrvlist!=[]):
                    retBoxes.append(boxestrvlist)
                    print(boxestrvlist)
                if(boxestrvlist==[]):
                    image_np = 255 - originalPhoto

                    image_np = rotateImage(image_np, 8)

                    nonzero = np.nonzero(image_np)

                    minx = min(nonzero[1])
                    maxx = max(nonzero[1])

                    miny = min(nonzero[0])
                    maxy = max(nonzero[0])

                    crop = image_np[miny:maxy, minx:maxx]
                    crop=255-crop

                    image_np = np.zeros((1600, 900,3), np.uint8)
                    image_np=255-image_np
                    x_offset = 450
                    y_offset = 800
                    image_np[y_offset:y_offset + crop.shape[0], x_offset:x_offset + crop.shape[1]] = crop
                    if image_np is None:
                        print("error reading file " + image_path)
                        continue
                        # end if

                        # Definite input and output Tensors for detection_graph
                    image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
                    # Each box represents a part of the image where a particular object was detected.
                    detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
                    # Each score represent how level of confidence for each of the objects.
                    # Score is shown on the result image, together with the class label.
                    detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
                    detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')
                    num_detections = detection_graph.get_tensor_by_name('num_detections:0')

                    # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
                    image_np_expanded = np.expand_dims(image_np, axis=0)
                    # Actual detection.
                    (boxes, scores, classes, num) = sess.run(
                        [detection_boxes, detection_scores, detection_classes, num_detections],
                        feed_dict={image_tensor: image_np_expanded})
                    # Visualization of the results of a detection.

                    image, boxestr = vis_util.visualize_boxes_and_labels_on_image_array(image_np,
                                                                                        np.squeeze(boxes),
                                                                                        np.squeeze(classes).astype(
                                                                                            np.int32),
                                                                                        np.squeeze(scores),
                                                                                        category_index,
                                                                                        use_normalized_coordinates=True,
                                                                                        line_thickness=2)

                    boxestrvlist = list(boxestr.values())
                    ####resize until box found

                    print(boxestrvlist)
                    retBoxes.append(boxestrvlist)
                    if(boxestrvlist==[]):
                        rotetCount += 1
                    else:
                        pass
                    cv2.imwrite(image_path, image_np)
                    # print result
                    #cv2.imshow("image_np black", image_np)
                    #cv2.waitKey()


    return retBoxes
            # end for
        # end with
    # end with
# end main
def checkIfBoxInsideWord(boxes,word,letter):
    for i in range(len(boxes)):
        box=boxes[i]
        if(box[0]>word[0]-0.04 and box[0]<word[2]+0.04 and box[1]>word[1]-0.04  and box[1]<word[3]+0.04 or box[2]>word[0]-0.04  and box[2]<word[2]+0.04 and box[3]>word[1]-0.04  and box[3]<word[3]+0.04):
            cword.append(letters[letter[i]][0])
    cword.append(" ")

#######################################################################################################################
def checkIfNecessaryPathsAndFilesExist():
    if not os.path.exists(TEST_IMAGE_DIR):
        print('ERROR: TEST_IMAGE_DIR "' + TEST_IMAGE_DIR + '" does not seem to exist')
        return False
    # end if

    # ToDo: check here that the test image directory contains at least one image

    if not os.path.exists(FROZEN_INFERENCE_GRAPH_LOC):
        print('ERROR: FROZEN_INFERENCE_GRAPH_LOC "' + FROZEN_INFERENCE_GRAPH_LOC + '" does not seem to exist')
        print('was the inference graph exported successfully?')
        return False
    # end if

    if not os.path.exists(LABELS_LOC):
        print('ERROR: the label map file "' + LABELS_LOC + '" does not seem to exist')
        return False
    # end if

    return True
# end function

#######################################
if __name__ == "__main__":
    main()
