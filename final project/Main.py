import imageProcessing as imp
import classifier as cls
import  os
imagequeue="ImageQueue/"
letters={"alef":"א","bet":"ב","gimel":"ג","dalet":"ד","hai":"ה","vav":"ו","zain":"ז","het":"ח","tet":"ט","yod":"י","chaf":"כ","lamed":"ל","mem":"מ","non":"נ",
         "samech":"ס","ain":"ע","pai":"פ","chadic":"צ","kof":"ק","resh":"ר","shin":"ש","taf":"ת","nons":"ן","mems":"ם","pais":"ף","chadics":"ץ","chafs":"ך"}
def main():
    try:
        os.mkdir('results')
    except:
        pass
    originalimages = []
    for file in os.listdir(imagequeue):
        if file.endswith(".jpg") or file.endswith(".jpeg") or file.endswith(".png"):
            originalimages.append(os.path.join(file))
    for image in originalimages:
        export=""
        if(imp.processImage(image)==1):
            print(image+" already processed")
        else:
            print(image+ " process completed successfully")
            t=cls.main(image)
            for img in t:
                if img==[]:
                    export+="?"
                if img==[""]:
                    export+="\n"
                elif img==[" "]:
                    export+=" "
                else:
                    for letter in img:
                        temp=letter[0].split(":")[0]
                        export+=letters[temp]
        f = open("results/"+image.split(".")[0]+".txt", "a")
        f.write(export)
        f.close()
        print(export)
if __name__ == '__main__':
    main()