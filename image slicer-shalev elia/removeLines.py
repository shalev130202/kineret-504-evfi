import cv2
import numpy as np
import skimage
import skimage.feature
import skimage.viewer
import sys

from matplotlib import pyplot as plt

def removeLines(imgname):
    img = cv2.imread(imgname)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    img = cv2.bitwise_not(img)
    th2 = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 15, -5)
    cv2.imwrite('th2.jpg',th2)


    horizontal = th2
    vertical = th2
    rows, cols = horizontal.shape

    # inverse the image, so that lines are black for masking
    horizontal_inv = cv2.bitwise_not(horizontal)
    # perform bitwise_and to mask the lines with provided mask
    masked_img = cv2.bitwise_and(img, img, mask=horizontal_inv)
    # reverse the image back to normal
    masked_img_inv = cv2.bitwise_not(masked_img)


    horizontalsize = int(cols / 20)
    horizontalStructure = cv2.getStructuringElement(cv2.MORPH_RECT, (horizontalsize, 1))
    horizontal = cv2.erode(horizontal, horizontalStructure, (-1, -1))
    horizontal = cv2.dilate(horizontal, horizontalStructure, (-1, -1))

    cv2.imwrite("horizontal.jpg", horizontal)


    verticalsize = int(rows / 30)
    verticalStructure = cv2.getStructuringElement(cv2.MORPH_RECT, (1, verticalsize))
    vertical = cv2.erode(vertical, verticalStructure, (-1, -1))
    vertical = cv2.dilate(vertical, verticalStructure, (-1, -1))

    cv2.imwrite("vertical.jpg", vertical)

    image = cv2.imread("th2.jpg")
    matrix = cv2.imread("horizontal.jpg")
    for row in range(0, np.size(matrix, 0)):
        for col in range(0, np.size(matrix, 1)):
            pixel = matrix[row][col]
            pixel = pixel.tolist()
            try:
                up = image[row + 1][col]
            except:
                continue
            try:
                down = image[row - 1][col]
            except:
                continue
            try:
                left=image[row ][col+1]
            except:
                continue
            try:
                right=image[row ][col-1]
            except:
                continue
            if (pixel >= [1, 1, 1]):
                image[row][col] = [0, 0, 0]
                if up.tolist() != None or down.tolist() != None or left.tolist()!=None or right.tolist()!=None:
                    pixel = up.tolist()
                    if (pixel >= [1, 1, 1]):
                        image[row + 1][col] = [0, 0, 0]
                    pixel = down.tolist()
                    if (pixel >= [1, 1, 1]):
                        image[row - 1][col] = [0, 0, 0]
                    pixel = left.tolist()
                    if (pixel >= [1, 1, 1]):
                        image[row ][col+1] = [0, 0, 0]
                    pixel = right.tolist()
                    if (pixel >= [1, 1, 1]):
                        image[row][col-1] = [0, 0, 0]


    originalimage=image
    i=1


    matrix = cv2.imread("vertical.jpg")
    for row in range(0, np.size(matrix, 0)):
        for col in range(0, np.size(matrix, 1)):
            pixel = matrix[row][col]
            pixel = pixel.tolist()
            try:
                up = image[row + 1][col]
            except:
                continue
            try:
                down = image[row - 1][col]
            except:
                continue
            try:
                left = image[row][col + 1]
            except:
                continue
            try:
                right = image[row][col - 1]
            except:
                continue
            if (pixel >= [1, 1, 1]):
                image[row][col] = [0, 0, 0]
                if up.tolist() != None or down.tolist() != None or left.tolist() != None or right.tolist() != None:
                    pixel = up.tolist()
                    if (pixel >= [1, 1, 1]):
                        image[row + 1][col] = [0, 0, 0]
                    pixel = down.tolist()
                    if (pixel >= [1, 1, 1]):
                        image[row - 1][col] = [0, 0, 0]
                    pixel = left.tolist()
                    if (pixel >= [1, 1, 1]):
                        image[row][col + 1] = [0, 0, 0]
                    pixel = right.tolist()
                    if (pixel >= [1, 1, 1]):
                        image[row][col - 1] = [0, 0, 0]

    cv2.imshow("before remove noise",image)
    img = image


    blur = cv2.GaussianBlur(img, (11, 11), 0)
    thresh = cv2.threshold(blur, 80, 255, cv2.THRESH_BINARY)[1]
    mid = cv2.medianBlur(thresh, 5)
    cv2.imshow("mid",mid)
    cv2.imshow('original', img)
    cv2.imshow('output', thresh)
#before
    #kernel = np.ones((2, 2), np.uint8)
    kernel = np.ones((2,1), np.uint8)
    #opening = cv2.morphologyEx(image, cv2.MORPH_OPEN, kernel)

    dilation = cv2.erode(thresh, kernel, iterations=1)
    dilation = cv2.bitwise_not(mid)

    imgname=imgname.replace(".jpg","")
    imgname=imgname.replace(".jpeg", "")
    imgname=imgname.replace(".png", "")
    #tag the image after processing
    imgname+="evfiap"
    imgname+=".png"
    cv2.imwrite(imgname, dilation)

    #cv2.waitKey(0)
    cv2.destroyAllWindows()
