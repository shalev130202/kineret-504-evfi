import cv2
import numpy as np
from collections import Counter
import os
import removeLines as rl
test_image_folder="D:/temp/image slicer-shalev elia/words"

def resizeImg(imgName):
   img = cv2.imread(imgName, cv2.IMREAD_UNCHANGED)

   scale_percent = 250  # percent of original size
   width = int(img.shape[1] * scale_percent / 100)
   height = int(img.shape[0] * scale_percent / 100)
   dim = (width, height)
   # resize image
   resized = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)
   cv2.imwrite(imgName, resized)

def new_line(min,max,n,folder,img,width,original):
   oim=cv2.imread("toScan/"+original)
   im = cv2.imread(img)
   name=folder+"/"+'l'+str(n)+".jpg"
   result=im[min:min+(max-min),0:width]
   oresult=oim[min:min+(max-min),0:width]
   oname = folder + "/"+"original/" + 'l' + str(n) + ".jpg"
   cv2.imwrite(name, result)
   cv2.imwrite(oname,oresult)

def new_word(min,max,n,folder,img,high):
   if max-min<5:
      return
   im = cv2.imread(img)
   name = test_image_folder + "/" +folder+'/'+ 'w' + str(n) + ".jpg"
   try:
      result=im[0:high,min:min+(max-min)]
   except:
      result = im[0:high, min:min + (max - min )]
   cv2.imwrite(name, result)
def hirizontal_histogram(img,original):
   j = 0
   lastwhitepixel = -2
   saveBorderLines = []
   sumOfZero = []

   fullPath="toScan/"+img
   im = cv2.imread(fullPath, cv2.IMREAD_GRAYSCALE)
   height, width = im.shape
   # הופף את הצבעים
   im = 255 - im
   proj = np.sum(im, 1)
   maxValue = np.max(proj)
   graphWidth = 500
   result = np.zeros((proj.shape[0], 500))
   for row in range(im.shape[0]):
      cv2.line(result, (0, row), (int(proj[row] * graphWidth / maxValue), row), (255, 255, 255), 1)
   cv2.imwrite('result.png', result)

   im = cv2.imread('result.png', cv2.IMREAD_GRAYSCALE)

   enter = False
   for i in im:
      n = i.tolist()
      sumOfZero.append(n.count(0))

   c = Counter(sumOfZero)

   mc = c.most_common(1)[0][0]
   for i in im:
      n = i.tolist()
      zerocount=n.count(0)
      if (zerocount < mc):
         if lastwhitepixel + 1 != j:
            saveBorderLines.append(j)
         lastwhitepixel = j

         enter = True
      elif enter == True:
         enter = False
         saveBorderLines.append(j)

      j += 1
   count = len(saveBorderLines)
   if count%2!=0:
      saveBorderLines.append(j)
   print(int(count / 2), "lines found")
   maxValue = 1
   i = 0
   for b in range(0, int(count / 2)):
      if(saveBorderLines[maxValue]-saveBorderLines[i]<5):
         i += 2
         maxValue += 2
         continue
      new_line(saveBorderLines[i], saveBorderLines[maxValue], b, img,fullPath,width,original)
      i += 2
      maxValue += 2

#להמשיך מהחלק של חלוקת התמונה לפי הגרף
def vertical_histogram(img,folder,word_count,original):
   j = 0
   b=0
   curr_word_count=word_count
   lastwhitepixel = -1
   saveBorderLines = []
   sumOfZero = []
   im = cv2.imread(img, cv2.IMREAD_GRAYSCALE)
   height, width = im.shape
   # הופף את הצבעים
   im = 255 - im
   proj = np.sum(im, 0)
   maxValue = np.max(proj)
   graphWidth = 500
   result = np.zeros((proj.shape[0], 500))
   for row in range(im.shape[1]):
      cv2.line(result, (0, row), (int(proj[row] * graphWidth / maxValue), row), (255, 255, 255), 1)
   cv2.imwrite('result2.png', result)

   im = cv2.imread('result2.png', cv2.IMREAD_GRAYSCALE)
   im = 255 - im



   enter = False
   for i in im:
      n = i.tolist()
      sumOfZero.append(n.count(0))
   count = Counter(sumOfZero)
   mc = count.most_common(2)[1][1]
   if (sumOfZero[0]>mc):
      saveBorderLines.append(0)
   for i in im:
      n = i.tolist()
      zerocount=n.count(0)
      if ( zerocount< mc + 2):
         if lastwhitepixel + 1 != j:
            saveBorderLines.append(j)
         lastwhitepixel = j
         enter = True
      elif enter == True:
         enter = False
         saveBorderLines.append(j)

      j += 1
   count = len(saveBorderLines)
   print(int(count / 2), "words found")
   maxValue = 1
   i = 0
   b=curr_word_count+int(count / 2)
   while b!=curr_word_count:
      new_word(saveBorderLines[i], saveBorderLines[maxValue], b,folder,original,height)
      word_count += 1
      i += 2
      maxValue += 2
      b-=1
   im = cv2.imread(img)
   name = test_image_folder + "/" + folder + '/' + 'w' + str(curr_word_count+int(count / 2)+1) + "endLine"
   cv2.imwrite(name, result)
   return curr_word_count+int(count / 2)+2
def main():

   word_count=0
   originalimages=[]
   images = []
   #count image to scan
   for file in os.listdir("toScan"):
      if file.endswith("evfiap.png"):
         continue
      if file.endswith(".jpg") or file.endswith(".jpeg") or file.endswith(".png"):
         originalimages.append(os.path.join(file))
   #hirizontal_histogram
   for image in originalimages:
      rl.removeLines("toScan" + "/" + image)

   for file in os.listdir("toScan"):
      if  file.endswith("evfiap.png"):
         images.append(os.path.join(file))
   i=0
   for image in images:
      if not os.path.exists(image):
         os.mkdir(image)
         os.mkdir(image+"/original")
         hirizontal_histogram(image,originalimages[i])
      i+=1
   for image in images:
      os.mkdir(test_image_folder+'/'+image)
   #vertical_histogram
   for image in images:
      word_count=0
      lines=[]
      for file in os.listdir(image):
         if file.endswith(".jpg"):
            lines.append(os.path.join(file))
      for line in lines:
         word_count=vertical_histogram(image+"/"+line,image,word_count,image+"/"+"original/"+line)
   toResize=[]
   dirs = filter(os.path.isdir, os.listdir(test_image_folder))
   for dir in dirs:
      for file in os.listdir(test_image_folder+'/'+dir):
         toResize.append(test_image_folder+'/'+dir+'/'+file)
   for file in toResize:
      resizeImg(file)





if __name__ == '__main__':
   main()
